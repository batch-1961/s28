// db.users.insertOne({
// 
//     
// 
//     "username": "dahyunTwice",
// 
//     "password": "dahyunKim"
// 
//     
// 
//     })
// 
// db.users.insertOne({
// 
//     
// 
//     "username": "gokusan",
// 
//     "password": "gokonKim"
// 
//     
// 
//     })
// 
//insert multiple documents at once

// db.users.insertMany(
//         [
//          {
//              "username": "pablo123",
//              "password": "123paul"
//          },
//          {
//              "username": "pedro99",
//              "password": "iampeter99"
//          }
// ])
// 

// 
// 
// 
// 
// db.product.insertMany(
//        [
//          {
//              "name": "Whey Protein",
//              "description": "'Substitute to protein intake",
//              "price":  3500,
//          },
//          {
//              "name": "Samsung S11",
//              "description": "Cellphone",
//              "price":  50000,
//          },
//          
//         {
//              "name": "I-phone pro max",
//              "description": "Cellphone",
//              "price":  40000
//          }
//  ])
//  

//read retrieve
// 
// db.users.find()
// db.collection.find() return all docs in the collection

//db.collection.find({"criteria":"Value"})

// db.users.find({"username": "pedro99"})

// db.cars.insertMany(
//         [
//           {
//               "name": "Vios",
//               "brand": "Toyota",
//               "type": "Sedan",
//               "price":  1500000,
//           },
//           {
//               "name": "Tamaraw FX",
//               "brand": "Toyota",
//               "type": "auv",
//               "price":  750000,
//           },
//           {
//               "name": "City",
//               "brand": "Honda",
//               "type": "sedan",
//               "price":  1600000,
//           }
//           
//        ])
//  
// db.cars.find({"type": "sedan"})
// db.cars.find({"brand": "Toyota"})

// db.cars.findOne({}) - find/return the first item in the collection
// db.cars.findOne({})


// db.cars.findOne({"type":"Sedan"}) find/return first item that matches in criteria
// db.cars.findOne({"type":"Sedan"})

// db.users.updateOne({"username":"pedro99"},{$set:{"username":"peter99"}})

// db.users.update({},{$set:{"username":"updateUsername"}}) first item update
// db.users.update({"username":"pablo123"},{$set:{"isAdmin":true}}) update yun field 

//db.users.updateMany({},{$set:{"isAdmin":true}}) 
// dagdag lahat pag wala yun criteria

// db.cars.updateMany({"type": "sedan"},{$set:{"price":1000000}})
// 
//  lahat na may same criterai mag uupdate"
// DELETE

// db.product.deleteOne({}) delete first item

// db.cars.deleteOne({"brand":"Toyota"})

//delete the first item that match the criteria

// db.users.deleteMany({"isAdmin":true})

//deletes all with the criteria

db.cars.deleteMany({})